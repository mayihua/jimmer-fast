package com.example.jimmerfast;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JimmerFastApplication {

    public static void main(String[] args) {
        SpringApplication.run(JimmerFastApplication.class, args);
    }

}
