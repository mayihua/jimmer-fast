package com.example.jimmerfast.service;

import com.example.jimmerfast.entity.Resource;
import com.example.jimmerfast.util.JimmerCrudService;

import java.util.List;

public interface ResourceService extends JimmerCrudService<Resource,Long> {
    List<Resource> userMenus(Long userId);
}
