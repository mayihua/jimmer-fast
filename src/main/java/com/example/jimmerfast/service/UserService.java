package com.example.jimmerfast.service;

import com.example.jimmerfast.entity.User;
import com.example.jimmerfast.util.JimmerCrudService;
import org.springframework.stereotype.Service;

public interface UserService extends JimmerCrudService<User,Long> {
    User currentUser();
}
