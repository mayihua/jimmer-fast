package com.example.jimmerfast.service;

import com.example.jimmerfast.entity.Resource;
import com.example.jimmerfast.entity.Role;
import com.example.jimmerfast.util.JimmerCrudService;

import java.util.List;

public interface RoleService extends JimmerCrudService<Role,Long> {
    void authorize(Long roleId, List<Long> resourceIds);
}
