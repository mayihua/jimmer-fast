package com.example.jimmerfast.service.impl;

import cn.dev33.satoken.stp.StpUtil;
import com.example.jimmerfast.entity.User;
import com.example.jimmerfast.entity.dto.UserInput;
import com.example.jimmerfast.entity.dto.UserSpecification;
import com.example.jimmerfast.service.UserService;
import com.example.jimmerfast.shape.UserShape;
import com.example.jimmerfast.util.JimmerCrudServiceImpl;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl extends JimmerCrudServiceImpl<User,Long> implements UserService {
    @Override
    public User currentUser() {
//        UserInput
        String loginId = StpUtil.getLoginId().toString();
        return findById(Long.valueOf(loginId), UserShape.PASSWORD_LESS);
    }
}
