package com.example.jimmerfast.service.impl;

import com.example.jimmerfast.entity.Role;
import com.example.jimmerfast.entity.RoleDraft;
import com.example.jimmerfast.service.RoleService;
import com.example.jimmerfast.util.JimmerCrudServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class RoleServerImpl extends JimmerCrudServiceImpl<Role, Long> implements RoleService {

    @Override
    public void authorize(Long roleId, List<Long> resourceIds) {
        sqlClient.save(
                RoleDraft.$.produce(role -> {
                    role.setId(roleId);
                    role.setResourceIds(resourceIds);
                })
        );
    }
}
