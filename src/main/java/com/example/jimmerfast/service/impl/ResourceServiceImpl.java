package com.example.jimmerfast.service.impl;

import com.example.jimmerfast.entity.Resource;
import com.example.jimmerfast.entity.ResourceFetcher;
import com.example.jimmerfast.entity.ResourceTable;
import com.example.jimmerfast.service.ResourceService;
import com.example.jimmerfast.util.JimmerCrudServiceImpl;
import org.babyfish.jimmer.client.FetchBy;
import org.babyfish.jimmer.sql.ast.table.spi.AbstractTypedTable;
import org.babyfish.jimmer.sql.fetcher.RecursiveListFieldConfig;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.example.jimmerfast.entity.Resource.TYPE_MENU;
import static com.example.jimmerfast.shape.ResourceShape.TREE_FETCHER;

@Service
public class ResourceServiceImpl extends JimmerCrudServiceImpl<Resource,Long> implements ResourceService {

    @Override
    public List<Resource> userMenus(Long userId) {
        return query().where(
                        ResourceTable.$
                                .asTableEx()
                                .roles()
                                .users()
                                .id().
                                eq(userId), ResourceTable.$.parent().isNull(), ResourceTable.$.type().eq(TYPE_MENU))
                .select(ResourceTable.$.fetch(
                        TREE_FETCHER
                )).distinct().execute();
    }
}
