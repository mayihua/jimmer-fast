package com.example.jimmerfast.error;

import org.babyfish.jimmer.error.ErrorFamily;
import org.babyfish.jimmer.error.ErrorField;

@ErrorFamily
public enum UserLoginErrorCode {
    @ErrorField(name = "msg", type = String.class)
    USERNAME_NOT_FOUND,

    @ErrorField(name = "msg", type = String.class)
    ILLEGAL_USERNAME_OR_PASSWORD
}