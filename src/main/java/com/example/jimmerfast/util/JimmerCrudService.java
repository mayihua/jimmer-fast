package com.example.jimmerfast.util;

import org.babyfish.jimmer.Input;
import org.babyfish.jimmer.sql.ast.mutation.DeleteResult;
import org.babyfish.jimmer.sql.ast.mutation.MutableUpdate;
import org.babyfish.jimmer.sql.ast.mutation.SimpleEntitySaveCommand;
import org.babyfish.jimmer.sql.ast.mutation.SimpleSaveResult;
import org.babyfish.jimmer.sql.ast.query.ConfigurableRootQuery;
import org.babyfish.jimmer.sql.ast.query.Example;
import org.babyfish.jimmer.sql.ast.query.MutableRootQuery;
import org.babyfish.jimmer.sql.ast.table.Table;
import org.babyfish.jimmer.sql.ast.table.spi.TableProxy;
import org.babyfish.jimmer.sql.fetcher.Fetcher;
import org.jetbrains.annotations.NotNull;

import java.util.Collection;
import java.util.List;


public interface JimmerCrudService<E, ID> {
    SimpleSaveResult<E> insert(E entity);

    default SimpleSaveResult<E> insert(Input<E> input){
        return insert(input.toEntity());
    }

    SimpleSaveResult<E> update(E entity);
    default SimpleSaveResult<E> update(Input<E> input){
        return update(input.toEntity());
    }

    SimpleSaveResult<E> save(E entity);

    default SimpleSaveResult<E> save(Input<E> input){
        return save(input.toEntity());
    }

    SimpleEntitySaveCommand<E> saveCommand(E entity);
    default SimpleEntitySaveCommand<E> saveCommand(Input<E> input){
        return saveCommand(input.toEntity());
    }

    DeleteResult deleteById(ID id);

    DeleteResult deleteByIds(Collection<ID> list);

    E findById(ID id);

    E findById(ID id, Fetcher<E> fetcher);

    List<E> findByIds(Collection<ID> idList);

    List<E> findAll();

    List<E> findByIds(Collection<ID> idList, Fetcher<E> fetcher);

    List<E> findAll(Fetcher<E> fetcher);

    @NotNull MutableRootQuery<? extends TableProxy<E>> query();

    PageResult<E> page(PageParam pageParam, ConfigurableRootQuery<? extends Table<E>, E> condition);

    PageResult<E> page(PageParam pageParam);
    PageResult<E> page(PageParam pageParam, Fetcher<E> fetcher);

    PageResult<E> pageByExample(PageParam pageParam, Example<E> example);
    PageResult<E> pageByExample(PageParam pageParam, Example<E> example, Fetcher<E> fetcher);


    List<E> findByExample(Example<E> example);
    List<E> findByExample(Example<E> example, Fetcher<E> fetcher);

    MutableUpdate createUpdate();
}
