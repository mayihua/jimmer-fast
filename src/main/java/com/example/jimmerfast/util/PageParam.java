package com.example.jimmerfast.util;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class PageParam {
    private int currentPage;
    private int pageSize;
}
