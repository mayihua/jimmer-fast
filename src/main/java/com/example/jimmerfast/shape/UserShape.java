package com.example.jimmerfast.shape;

import com.example.jimmerfast.entity.UserFetcher;

public interface UserShape {
    UserFetcher PASSWORD_LESS = UserFetcher.$.allScalarFields().password(false);
    UserFetcher PASSWORD_LESS_WITH_ROLE_IDS = PASSWORD_LESS.roleIds();
}
