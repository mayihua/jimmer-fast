package com.example.jimmerfast.shape;

import com.example.jimmerfast.entity.ResourceFetcher;
import org.babyfish.jimmer.sql.fetcher.RecursiveListFieldConfig;

public interface ResourceShape {
    ResourceFetcher TREE_FETCHER = ResourceFetcher.$
            .allScalarFields()
            .children(
                    ResourceFetcher.$.allScalarFields(),
                    RecursiveListFieldConfig::recursive
            );

}
