package com.example.jimmerfast.controller;

import cn.hutool.core.util.StrUtil;
import cn.hutool.crypto.digest.DigestUtil;
import com.example.jimmerfast.entity.User;
import com.example.jimmerfast.entity.UserTable;
import com.example.jimmerfast.entity.dto.UserInput;
import com.example.jimmerfast.entity.dto.UserSpecification;
import com.example.jimmerfast.service.UserService;
import com.example.jimmerfast.shape.UserShape;
import com.example.jimmerfast.util.JimmerUtil;
import com.example.jimmerfast.util.PageParam;
import com.example.jimmerfast.util.PageResult;
import com.example.jimmerfast.util.validator.ValidatorUtil;
import com.example.jimmerfast.util.validator.group.AddGroup;
import com.example.jimmerfast.util.validator.group.DefaultGroup;
import com.example.jimmerfast.util.validator.group.UpdateGroup;
import lombok.AllArgsConstructor;
import org.babyfish.jimmer.client.FetchBy;
import org.babyfish.jimmer.sql.ast.query.ConfigurableRootQuery;
import org.babyfish.jimmer.sql.ast.query.Example;
import org.babyfish.jimmer.sql.ast.query.MutableRootQuery;
import org.babyfish.jimmer.sql.ast.table.spi.TableProxy;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@AllArgsConstructor
@RequestMapping("user")
public class UserController {


    private final UserService userService;

    @GetMapping("userInfo")
    public @FetchBy(value = "PASSWORD_LESS", ownerType = UserShape.class) User userInfo() {
        return userService.currentUser();
    }

    @GetMapping("page")
    public PageResult<@FetchBy(value = "PASSWORD_LESS_WITH_ROLE_IDS", ownerType = UserShape.class) User> page(PageParam pageParam, UserSpecification specification) {
        ConfigurableRootQuery<? extends TableProxy<User>, User> query = userService.query().
                whereIf(StrUtil.isNotBlank(specification.getUsername()), UserTable.$.username().like(specification.getUsername()))
                .whereIf(StrUtil.isNotBlank(specification.getNickname()), UserTable.$.nickname().like(specification.getNickname())).select(UserTable.$.fetch(UserShape.PASSWORD_LESS_WITH_ROLE_IDS));
        return userService.page(pageParam, query);
    }


    @PostMapping("deleteByIds")
    public void deleteByIds(@RequestBody List<Long> ids) {
        userService.deleteByIds(ids);
    }

    @PostMapping("add")
    public void add(@RequestBody UserInput userInput) {
        ValidatorUtil.validate(userInput, AddGroup.class, DefaultGroup.class);
        handlePassword(userInput);
        userService.save(userInput);
    }

    @PostMapping("update")
    public void update(@RequestBody UserInput userInput) {
        ValidatorUtil.validate(userInput, UpdateGroup.class, DefaultGroup.class);
        handlePassword(userInput);
        userService.save(userInput);
    }

    private void handlePassword(UserInput userInput) {
        String rawPassword = userInput.getPassword();
        if (StrUtil.isNotBlank(rawPassword)) {
            userInput.setPassword(DigestUtil.md5Hex(rawPassword));
        }
    }
}
