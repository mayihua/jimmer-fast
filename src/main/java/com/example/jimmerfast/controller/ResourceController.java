package com.example.jimmerfast.controller;

import cn.dev33.satoken.stp.StpUtil;
import com.example.jimmerfast.entity.Resource;
import com.example.jimmerfast.entity.ResourceTable;
import com.example.jimmerfast.entity.dto.ResourceInput;
import com.example.jimmerfast.entity.dto.RoleInput;
import com.example.jimmerfast.entity.dto.UserInput;
import com.example.jimmerfast.service.ResourceService;
import com.example.jimmerfast.shape.ResourceShape;
import com.example.jimmerfast.util.validator.ValidatorUtil;
import com.example.jimmerfast.util.validator.group.AddGroup;
import com.example.jimmerfast.util.validator.group.DefaultGroup;
import com.example.jimmerfast.util.validator.group.UpdateGroup;
import lombok.RequiredArgsConstructor;
import org.babyfish.jimmer.client.FetchBy;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static com.example.jimmerfast.entity.Resource.TYPE_BUTTON;
import static com.example.jimmerfast.entity.Resource.TYPE_MENU;
import static com.example.jimmerfast.shape.ResourceShape.TREE_FETCHER;

@RestController("resource")
@RequiredArgsConstructor
public class ResourceController {

    private final ResourceService resourceService;

    @GetMapping("myMenus")
    public List<@FetchBy(value = "TREE_FETCHER", ownerType = ResourceShape.class) Resource> myMenus(){
        String loginId = StpUtil.getLoginId().toString();
        return resourceService.userMenus(Long.valueOf(loginId));
    }

    @GetMapping("tree")
    public List<@FetchBy(value = "TREE_FETCHER", ownerType = ResourceShape.class) Resource> tree(){
        return resourceService.query().where(
                ResourceTable.$.type().eq(TYPE_MENU),
                        ResourceTable.$.parent().isNull())
                .select(ResourceTable.$.fetch(
                        TREE_FETCHER
                )).distinct().execute();
    }

    @GetMapping("myButtons")
    public List<String> myButtons(){
        String loginId = StpUtil.getLoginId().toString();
        return resourceService.query()
                        .where(ResourceTable.$.asTableEx()
                                .roles()
                                .users().id().
                                eq(Long.valueOf(loginId)),
                                ResourceTable.$.type().eq(TYPE_BUTTON)
                        )
                .select(ResourceTable.$.permission()).distinct().execute();
    }

    @GetMapping("findIdsByRoleId")
    public List<Long> findIdsByRoleId(Long roleId){
        return resourceService.query()
                .where(ResourceTable.$.asTableEx()
                                .roles().id().
                                eq(roleId)
                )
                .select(ResourceTable.$.id()).distinct().execute();
    }


    @PostMapping("add")
    public void add(@RequestBody ResourceInput resourceInput) {
        ValidatorUtil.validate(resourceInput, AddGroup.class, DefaultGroup.class);

        System.out.println(resourceInput.toEntity());
        resourceService.save(resourceInput);
    }

    @PostMapping("update")
    public void update(@RequestBody ResourceInput resourceInput) {
        ValidatorUtil.validate(resourceInput, UpdateGroup.class, DefaultGroup.class);
        resourceService.save(resourceInput);
    }

    @PostMapping("deleteByIds")
    public void deleteByIds(@RequestBody List<Long> ids) {
        resourceService.deleteByIds(ids);
    }
}
