package com.example.jimmerfast.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.convert.converter.Converter;

import java.time.LocalDateTime;
import java.util.Date;

/**
 * @author mayihua
 */
@Configuration
public class DateConfig {

    @Bean
    public Converter<String, Date> dateConverter() {
        return new DateConverter();
    }

    @Bean
    public Converter<String, LocalDateTime> localDateTimeConverter() {
        return new LocalDateTimeConverter();
    }
}
