package com.example.jimmerfast.config;

import cn.hutool.core.date.DateUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.convert.converter.Converter;

import java.util.Date;

/**
 * 日期转换类
 * 将标准日期、标准日期时间、时间戳转换成Date类型
 */
/*@Deprecated*/
@Slf4j
public class DateConverter implements Converter<String, Date> {


    @Override
    public Date convert(String value) {
        log.info("转换日期：" + value);
        if (value.trim().isEmpty() || value.equalsIgnoreCase("null")) {
            return null;
        }

        value = value.trim();
        return DateUtil.parse(value);
    }
}
