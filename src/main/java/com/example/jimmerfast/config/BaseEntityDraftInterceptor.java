package com.example.jimmerfast.config;

import com.example.jimmerfast.entity.common.BaseEntityDraft;
import com.example.jimmerfast.entity.common.BaseEntityProps;
import org.babyfish.jimmer.ImmutableObjects;
import org.babyfish.jimmer.sql.DraftInterceptor;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
public class BaseEntityDraftInterceptor implements DraftInterceptor<BaseEntityDraft> {
    @Override
    public void beforeSave(BaseEntityDraft draft, boolean isNew) {
        LocalDateTime now = LocalDateTime.now();
        if (!ImmutableObjects.isLoaded(draft, BaseEntityProps.UPDATE_TIME)) {
            draft.setUpdateTime(now);
        }
        if (isNew && !ImmutableObjects.isLoaded(draft, BaseEntityProps.CREATE_TIME)) {
            draft.setCreateTime(now);
        }
    }
}
