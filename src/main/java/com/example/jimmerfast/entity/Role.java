package com.example.jimmerfast.entity;

import com.example.jimmerfast.entity.common.BaseEntity;
import org.babyfish.jimmer.sql.*;

import java.util.List;

@Entity
@Table(name = "sys_role")
public interface Role extends BaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    long id();

    @Key
    String code();

    String name();


    @ManyToMany(mappedBy = "roles")
    List<User> users();

    @ManyToMany(mappedBy = "roles")
    List<Resource> resources();

    @IdView("resources")
    List<Long> resourceIds();
}
