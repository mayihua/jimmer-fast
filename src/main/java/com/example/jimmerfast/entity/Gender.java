package com.example.jimmerfast.entity;

import org.babyfish.jimmer.sql.EnumItem;
import org.babyfish.jimmer.sql.EnumType;

@EnumType(EnumType.Strategy.NAME)
public enum Gender {

    @EnumItem(name = "M")
    M,

    @EnumItem(name = "F")
    F
}
