package com.example.jimmerfast.entity;

import com.example.jimmerfast.entity.common.BaseEntity;
import org.babyfish.jimmer.client.Doc;
import org.babyfish.jimmer.sql.*;

import javax.validation.constraints.Null;
import java.util.List;

@Entity
@Table(name = "sys_user")
public interface User extends BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    long id();

    @Key
    @Doc("用户名")
    String username();

    @Null
    String password();

    @Null
    String nickname();

    @Null
    String avatar();

    @Null
    Gender gender();

    @Null
    String email();

    @Null
    String mobile();
    @ManyToMany
    @JoinTable(
            name = "sys_user_role",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseColumns = @JoinColumn(name = "role_id")
    )
    List<Role> roles();

    @IdView("roles")
    List<Long> roleIds();
}
