package com.example.jimmerfast.entity.common;

import org.babyfish.jimmer.sql.MappedSuperclass;

import java.time.LocalDateTime;

@MappedSuperclass // ❶
public interface BaseEntity {
    LocalDateTime createTime();

    LocalDateTime updateTime();
}
