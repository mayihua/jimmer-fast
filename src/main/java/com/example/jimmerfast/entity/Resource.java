package com.example.jimmerfast.entity;

import com.example.jimmerfast.entity.common.BaseEntity;
import org.babyfish.jimmer.sql.*;
import org.jetbrains.annotations.Nullable;

import javax.validation.constraints.Null;
import java.util.List;

@Entity
@Table(name = "sys_resource")
public interface Resource extends BaseEntity {

    String TYPE_MENU = "menu";
    String TYPE_LINK = "link";
    String TYPE_BUTTON = "button";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    long id();

    @ManyToMany
    @JoinTable(
            name = "sys_role_resource",
            joinColumns = @JoinColumn(name = "resource_id"),
            inverseColumns = @JoinColumn(name = "role_id")
    )
    List<Role> roles();

    @IdView("roles")
    List<Long> roleIds();

    @Null // Null property, Java API requires this annotation, but kotlin API does not
    @Key
    @ManyToOne
    @OnDissociate(DissociateAction.DELETE)
    Resource parent();

    @OneToMany(mappedBy = "parent")
    List<Resource> children();

    @IdView("parent")
    @Null
    Long parentId();

    @Null
    String name();

    @Null
    String icon();


    /**
     * 显示名称
     */
    @Key
    String title();

    /**
     * 权限代码
     */
    @Null
    String permission();

    /**
     * 前端路径
     */
    @Null
    @Key
    String path();

    /**
     * 前端组件路径
     */
    @Null
    String component();

    /**
     * 排序
     */
    int sort();

    /**
     * 菜单类型
     */
    String type();
}
