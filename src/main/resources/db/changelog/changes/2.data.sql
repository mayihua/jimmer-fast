INSERT INTO `sys_resource` (`id`, `name`, `title`, `permission`, `path`, `component`, `icon`, `sort`, `type`, `parent_id`, `delete_time`, `create_time`, `update_time`, `create_by`, `update_by`) VALUES (1, 'Dashboard', '首页', 'sys:dashboard:view', '/dashboard', 'dashboard', NULL, 1, 'menu', NULL, 0, '2023-10-25 22:47:07', '2023-10-25 22:47:18', NULL, NULL);
INSERT INTO `sys_resource` (`id`, `name`, `title`, `permission`, `path`, `component`, `icon`, `sort`, `type`, `parent_id`, `delete_time`, `create_time`, `update_time`, `create_by`, `update_by`) VALUES (2, NULL, '系统管理', 'sys:manage:view', '/system', NULL, NULL, 2, 'menu', NULL, 0, '2023-10-25 22:47:11', '2023-10-19 22:47:22', NULL, NULL);
INSERT INTO `sys_resource` (`id`, `name`, `title`, `permission`, `path`, `component`, `icon`, `sort`, `type`, `parent_id`, `delete_time`, `create_time`, `update_time`, `create_by`, `update_by`) VALUES (3, 'AuthorityUser', '用户管理', 'sys:manage:view', '/system/user', 'system/user', NULL, 1, 'menu', 2, 0, '2023-10-25 22:47:14', '2023-10-28 22:47:26', NULL, NULL);
INSERT INTO `sys_resource` (`id`, `name`, `title`, `permission`, `path`, `component`, `icon`, `sort`, `type`, `parent_id`, `delete_time`, `create_time`, `update_time`, `create_by`, `update_by`) VALUES (4, 'AuthorityRole', '角色管理', 'sys:authority-role:view', '/system/role', 'system/role', NULL, 1, 'menu', 2, 0, '2023-10-25 22:47:14', '2023-10-28 22:47:26', NULL, NULL);
INSERT INTO `sys_resource` (`id`, `name`, `title`, `permission`, `path`, `component`, `icon`, `sort`, `type`, `parent_id`, `delete_time`, `create_time`, `update_time`, `create_by`, `update_by`) VALUES (5, 'AuthorityResource', '权限资源管理', 'sys:authority-resource:view', '/system/resource', 'system/resource', NULL, 1, 'menu', 2, 0, '2023-10-25 22:47:14', '2023-10-28 22:47:26', NULL, NULL);



INSERT INTO `sys_role` (`id`, `name`, `code`, `parent_id`, `delete_time`, `create_time`, `update_time`, `create_by`, `update_by`) VALUES (1, '管理员', 'admin', NULL, 0, '2023-10-22 19:05:58', '2023-10-22 19:06:02', NULL, NULL);


INSERT INTO `sys_role_resource` (`role_id`, `resource_id`) VALUES (1, 1);
INSERT INTO `sys_role_resource` (`role_id`, `resource_id`) VALUES (1, 2);
INSERT INTO `sys_role_resource` (`role_id`, `resource_id`) VALUES (1, 3);
INSERT INTO `sys_role_resource` (`role_id`, `resource_id`) VALUES (1, 4);
INSERT INTO `sys_role_resource` (`role_id`, `resource_id`) VALUES (1, 5);


INSERT INTO `sys_user` (`id`, `username`, `password`, `nickname`, `avatar`, `gender`, `email`, `mobile`, `delete_time`, `create_time`, `update_time`, `create_by`, `update_by`) VALUES (1, 'admin', '0192023A7BBD73250516F069DF18B500', '管理员1', NULL, 'M', NULL, NULL, 0, '2023-10-25 22:47:48', '2023-10-25 22:47:46', NULL, NULL);

INSERT INTO `sys_user_role` (`user_id`, `role_id`) VALUES (1, 1);