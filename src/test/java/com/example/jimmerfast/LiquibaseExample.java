package com.example.jimmerfast;

import liquibase.Liquibase;
import liquibase.database.Database;
import liquibase.database.DatabaseFactory;
import liquibase.database.jvm.JdbcConnection;
import liquibase.diff.DiffResult;
import liquibase.diff.compare.CompareControl;
import liquibase.diff.output.DiffOutputControl;
import liquibase.diff.output.changelog.DiffToChangeLog;
import liquibase.resource.ClassLoaderResourceAccessor;
import liquibase.serializer.ChangeLogSerializerFactory;

import java.io.FileOutputStream;
import java.sql.Connection;
import java.sql.DriverManager;

public class LiquibaseExample {

    public static void main(String[] args) {
        try {
            // 设置数据库连接信息
            String url = "jdbc:mysql://localhost:3306/demo";
            String username = "root";
            String password = "root";

            // 创建数据库连接
            Connection connection = DriverManager.getConnection(url, username, password);

            // 创建Liquibase数据库对象
            Database database = DatabaseFactory.getInstance().findCorrectDatabaseImplementation(new JdbcConnection(connection));

            Liquibase liquibase = new Liquibase("", new ClassLoaderResourceAccessor(), database);

            DiffOutputControl diffOutputControl = new DiffOutputControl();


            DiffResult diff = liquibase.diff(database, null, new CompareControl());
            DiffToChangeLog diffToChangeLog = new DiffToChangeLog(diff,diffOutputControl);
            diffToChangeLog.print(System.out, ChangeLogSerializerFactory.getInstance().getSerializer("yaml"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}